package dev.mattia.customJoin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        System.out.println("[CustomJoin] Started Plugin");
        this.getConfig().options().copyDefaults(true);
        saveConfig();
        Bukkit.getPluginManager().registerEvents(this, this);
        super.onEnable();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase("customjoin")) {
            if (sender instanceof Player) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("reload")) {
                        if (sender.hasPermission("customjoin.reload")) {
                            this.reloadConfig();
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("prefix") + " &bReloaded"));
                        } else {
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("prefix") + " " + this.getConfig().getString("error").replaceAll("%version%", this.getDescription().getVersion()).replaceAll("%server_version%", Bukkit.getBukkitVersion())));
                        }
                    }
                } else {
                    if (sender.hasPermission("customjoin.reload")) {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("prefix") + " &bUsage: /customjoin [reload]"));
                    } else {
                        assert this.getDescription().getDescription() != null;

                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("prefix") + " " + this.getConfig().getString("info").replaceAll("%version%", this.getDescription().getVersion()).replaceAll("%server_version%", Bukkit.getBukkitVersion())));
                    }
                }
            } else {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("reload")) {
                        this.reloadConfig();
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("prefix") + " &bReloaded"));
                    }
                }
            }
        }
        return super.onCommand(sender, command, label, args);
    }

    @Override
    public void onDisable() {
        System.out.println("[CustomJoin] Disabled Plugin");
        super.onDisable();
    }
    @EventHandler
    public void joinListener(PlayerJoinEvent e) {
        if (this.getConfig().getBoolean("join.enabled")) {
            e.setJoinMessage(null);
            Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("join.message").replaceAll("%user%", e.getPlayer().getDisplayName())));
        }
    }

    @EventHandler
    public void quitListener(PlayerQuitEvent e) {
        if (this.getConfig().getBoolean("quit.enabled")) {
            e.setQuitMessage(null);
            Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("quit.message").replaceAll("%user%", e.getPlayer().getDisplayName())));
        }
    }

}
