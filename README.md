# CustomJoin v1.1

CustomJoin is a simple light-weight plugin for your minecraft server that allows you to edit join/welcome and quit message with color codes and text styles

### ......................................................................................................................................................................................

## Author

**Mattia** - [ChieLLo__FSK](https://bitbucket.org/ChieLLo__FSK)


## License

This project is licensed under the MIT License - see the [LICENSE](https://bitbucket.org/MateeHash/lagreport/src/master/LICENSE) file for details.
